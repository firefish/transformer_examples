from sklearn.metrics import roc_auc_score

with open('res.txt.my_transformer_encoder.my_transformer_custom.compare_nums.beam4.alpha0.6.decodes', 'r') as pred_f, \
    open('res.txt.my_transformer_encoder.my_transformer_custom.compare_nums.beam4.alpha0.6.targets', 'r') as labels_f:
    pred = [float(x.rstrip()) for x in pred_f]
    labels = [int(float(x.rstrip())) for x in labels_f]
    print(roc_auc_score(labels, pred))
