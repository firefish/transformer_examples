#!/usr/bin/env bash
PROBLEM=compare_nums
MODEL=my_transformer_encoder
HPARAMS=my_transformer_custom

DATA_DIR=base/data
TMP_DIR=base/tmp
TRAIN_DIR=base/train

mkdir -p "$DATA_DIR" "$TMP_DIR" "$TRAIN_DIR"

t2t-datagen \
  --t2t_usr_dir=usr_dir \
  --data_dir="$DATA_DIR" \
  --tmp_dir="$TMP_DIR" \
  --problem="$PROBLEM"

t2t-trainer \
  --keep_checkpoint_max 99999 \
  --t2t_usr_dir=usr_dir \
  --data_dir="$DATA_DIR" \
  --problems="$PROBLEM" \
  --model="$MODEL" \
  --output_dir="$TRAIN_DIR" \
  --hparams_set="$HPARAMS" \
  --train_steps=500000 \
  --eval_steps=20
