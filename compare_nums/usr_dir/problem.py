import tensorflow as tf
import hashlib
from tensor2tensor.utils import registry
from tensor2tensor.utils import modality
from tensor2tensor.layers import common_layers
from tensor2tensor.data_generators import problem
from tensor2tensor.data_generators import generator_utils
from tensor2tensor.data_generators import text_encoder
from tensor2tensor.models.transformer import transformer_tiny, transformer_base


@registry.register_problem
class CompareNums(problem.Problem):
    @property
    def targeted_vocab_size(self):
        return 2 ** 15  # 32768

    def get_encoder(self, data_dir):
        encoder = generator_utils.get_or_generate_vocab_inner(
            data_dir, 'vocab.txt', self.targeted_vocab_size, self.doc_generator(data_dir, "train"))
        return encoder

    def doc_generator(self, data_dir, dataset, include_label=False):
        with open(data_dir + '/q1.txt', 'r') as q1_f, open(data_dir + '/q2.txt', 'r') as q2_f, open(
                data_dir + '/label.txt', 'r') as labels_f:
            for first, second, label in zip(q1_f, q2_f, labels_f):
                first, second = first.lower(), second.lower()
                if include_label:
                    yield first, second, label
                    yield second, first, label
                else:
                    yield first
                    yield second

    def generator(self, data_dir, tmp_dir, train):
        encoder = self.get_encoder(data_dir)
        for first, second, label in self.doc_generator(data_dir, "train", include_label=True):
            hash = int(hashlib.sha256(first.encode('utf-8')).hexdigest(), 16) ^ int(
                hashlib.sha256(second.encode('utf-8')).hexdigest(), 16)
            if (hash % 100 != 0) == train:
                yield {
                    "inputs": encoder.encode(first) + [text_encoder.EOS_ID],
                    "inputs_second": encoder.encode(second) + [text_encoder.EOS_ID],
                    "targets": [float(label)],
                }

    def generate_data(self, data_dir, tmp_dir, task_id=-1):
        train_paths = self.training_filepaths(data_dir, 1, shuffled=False)
        dev_paths = self.dev_filepaths(data_dir, 1, shuffled=False)
        generator_utils.generate_dataset_and_shuffle(
            self.generator(data_dir, tmp_dir, True), train_paths,
            self.generator(data_dir, tmp_dir, False), dev_paths,
            shuffle=True
        )

    def hparams(self, defaults, unused_model_hparams):
        p = defaults
        p.input_modality = {
            "inputs": (registry.Modalities.SYMBOL, self.targeted_vocab_size),
            "inputs_second": (registry.Modalities.SYMBOL, self.targeted_vocab_size),
        }
        p.target_modality = ('class_label:binary', 1)
        p.input_space_id = problem.SpaceID.EN_TOK
        p.target_space_id = problem.SpaceID.GENERIC

    def feature_encoders(self, data_dir):
        return {
            "inputs": self.get_encoder(data_dir),
            "inputs_second": self.get_encoder(data_dir),
            "targets": IdentityEncoder(),
        }

    def example_reading_spec(self):
        data_fields = {
            "inputs": tf.VarLenFeature(tf.int64),
            "inputs_second": tf.VarLenFeature(tf.int64),
            "targets": tf.FixedLenFeature([1], tf.float32),
        }
        data_items_to_decoders = None
        return data_fields, data_items_to_decoders


# Hack to use multiple inputs
def my_standardize_shapes(features, batch_size=None):
    for fname in {"inputs", "targets", "inputs_second"}.intersection(features):
        f = features[fname]
        while len(f.get_shape()) < 4:
            f = tf.expand_dims(f, axis=-1)
        features[fname] = f
    assert not batch_size
    return features


problem.standardize_shapes = my_standardize_shapes


# /Hack

@registry.register_hparams
def my_transformer_custom():
    # hparams = transformer_base()
    # hparams.num_hidden_layers = 1
    # hparams.hidden_size = 32
    # hparams.filter_size = 32
    # hparams.max_length = 128
    # hparams.num_heads = 4
    # hparams.learning_rate_warmup_steps = 4000
    # hparams.batch_size = 2048
    # hparams.learning_rate = 0.025
    # return hparams
    hparams = transformer_tiny()
    return hparams


class IdentityEncoder(object):
    def encode(self, x):
        return x

    def decode(self, x):
        return x


@registry.register_class_label_modality("binary")
class ClassLabelModality(modality.Modality):
    @property
    def name(self):
        return "class_label_modality_%d_%d" % (self._vocab_size, self._body_input_depth)

    def bottom(self, x):
        assert False

    def targets_bottom(self, x):
        with tf.variable_scope(self.name):
            return tf.zeros([common_layers.shape_list(x)[0], 1, 1, self._body_input_depth])

    def top(self, body_output, _):
        """Transform inputs from model space to target space.

        Average over inner dims and a linear layer to logits.

        Args:
          body_output: A Tensor with shape [batch, ?, ?, body_output_size].

        Returns:
          a Tensors, each with shape [batch_size, ?, ?, vocab_size]
        """
        with tf.variable_scope(self.name):
            x = body_output
            print('before x', x)
            x = tf.layers.dense(x, 1)
            print('after x', x)
            res = tf.layers.dense(
                tf.concat([
                    tf.reduce_mean(x, axis=[1, 2], keep_dims=True),
                    tf.reduce_min(x, axis=[1, 2], keep_dims=True),
                    tf.reduce_max(x, axis=[1, 2], keep_dims=True),
                    tf.reduce_sum(x, axis=[1, 2], keep_dims=True),
                ], axis=3), 1)
            print('res', res)
            return tf.expand_dims(res, 3)

    def loss(self, logits, targets):
        print('targets', targets)
        print('logits', tf.squeeze(logits, axis=3))
        return tf.losses.log_loss(
            tf.cast(targets, tf.float32),
            tf.sigmoid(tf.squeeze(logits, axis=3))
        ), tf.constant(0., tf.float32)
