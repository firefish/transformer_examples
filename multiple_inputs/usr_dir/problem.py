# coding=utf-8
# Copyright 2017 The Tensor2Tensor Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""IMDB Sentiment Classification Problem."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tarfile

# Dependency imports

from tensor2tensor.data_generators import generator_utils
from tensor2tensor.data_generators import problem
from tensor2tensor.data_generators import text_encoder
from tensor2tensor.utils import registry

import tensorflow as tf

# End-of-sentence marker.
EOS = text_encoder.EOS_ID


@registry.register_problem
class MyTextToText(problem.Problem):
  @property
  def num_shards(self):
    return 1

  def generator(self, train):
    up_to = 10000
    for i in range(up_to):
      if train == (0 != i % 11):
        s = str(i)
        s = str(up_to)[1+len(s):] + s  # 10000[1:] -> 0000[len('12'):] -> 00 + 12 = 0012
        yield {
            "inputs": text_encoder.ByteTextEncoder().encode(s) + [EOS],
            "targets": text_encoder.ByteTextEncoder().encode(s) + [EOS],
        }

  def generate_data(self, data_dir, tmp_dir, task_id=-1):
    train_paths = self.training_filepaths(data_dir, self.num_shards, shuffled=False)
    dev_paths = self.dev_filepaths(data_dir, 1, shuffled=False)
    generator_utils.generate_dataset_and_shuffle(
        self.generator(True), train_paths,
        self.generator(False), dev_paths)

  def hparams(self, defaults, unused_model_hparams):
    p = defaults
    p.input_modality = {
        "inputs": (registry.Modalities.SYMBOL, 258)
    }
    p.target_modality = (registry.Modalities.SYMBOL, 258)
    p.input_space_id = problem.SpaceID.EN_CHR
    p.target_space_id = problem.SpaceID.EN_CHR

  def feature_encoders(self, data_dir):
    return {
        "inputs": text_encoder.ByteTextEncoder(),
        "targets": text_encoder.ByteTextEncoder(),
    }

  def example_reading_spec(self):
    data_fields = {
        "inputs": tf.VarLenFeature(tf.int64),
        "targets": tf.VarLenFeature(tf.int64),
    }
    data_items_to_decoders = None
    return (data_fields, data_items_to_decoders)

