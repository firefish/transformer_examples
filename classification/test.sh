#!/usr/bin/env bash
PROBLEM=my_sentiment_imdb
MODEL=my_transformer_encoder
HPARAMS=my_transformer_custom

DATA_DIR=base/data
TMP_DIR=base/tmp
TRAIN_DIR=base/train

mkdir -p "$DATA_DIR" "$TMP_DIR" "$TRAIN_DIR"

python t2t-decoder.py \
  --t2t_usr_dir=usr_dir \
  --data_dir=$DATA_DIR \
  --problems=$PROBLEM \
  --model=$MODEL \
  --hparams_set=$HPARAMS \
  --output_dir=$TRAIN_DIR \
  --decode_to_file="res.txt"
