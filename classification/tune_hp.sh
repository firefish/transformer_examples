#!/usr/bin/env bash
PROBLEM=my_sentiment_imdb
MODEL=my_transformer_encoder
HPARAMS=my_transformer_env

for warmup in 8000 4000; do
  for layers in 2 4; do #-1
    for heads in 1 2 4 8; do
      for rate in 0.025 0.0125 0.05; do
        for dimensions in 32 64 128; do
          for length in 512 1024; do
            NAME="warmup=$warmup,layers=$layers,heads=$heads,rate=$rate,dimensions=$dimensions,length=$length"
            DATA_DIR=base/data
            TMP_DIR=base/tmp
            TRAIN_DIR="base/train,$NAME"

            mkdir -p "$DATA_DIR" "$TMP_DIR" "$TRAIN_DIR"

            warmup=$warmup layers=$layers heads=$heads rate=$rate dimensions=$dimensions length=$length \
              t2t-trainer \
              --t2t_usr_dir=usr_dir \
              --data_dir="$DATA_DIR" \
              --problems="$PROBLEM" \
              --model="$MODEL" \
              --output_dir="$TRAIN_DIR" \
              --hparams_set="$HPARAMS" \
              --train_steps=24000 \
              --eval_steps=1000 \
              --local_eval_frequency=1000 \
              2>"$TRAIN_DIR/train.log"
          done
        done
      done
    done
  done
done