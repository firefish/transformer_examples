import tensorflow as tf
from tensor2tensor.utils import registry
from tensor2tensor.utils import modality
from tensor2tensor.layers import common_layers
from tensor2tensor.data_generators import problem
from tensor2tensor.data_generators import generator_utils
from tensor2tensor.data_generators import text_encoder
from tensor2tensor.models.transformer import transformer_base


@registry.register_problem
class CompareNums(problem.Problem):
    def generator(self, train):
        up_to = 1000
        for i in range(up_to):
            for j in range(up_to):
                if train != (0 != (i + j) % 7):
                    si = (str(up_to) + str(i))[-len(str(up_to - 1)):]
                    sj = (str(up_to) + str(j))[-len(str(up_to - 1)):]
                    res = {
                        "inputs": text_encoder.ByteTextEncoder().encode(si) + [text_encoder.EOS_ID],
                        "inputs_second": text_encoder.ByteTextEncoder().encode(sj) + [text_encoder.EOS_ID],
                        "targets": [1. if i >= j else 0.],
                    }
                    yield res

    def generate_data(self, data_dir, tmp_dir, task_id=-1):
        train_paths = self.training_filepaths(data_dir, 1, shuffled=False)
        dev_paths = self.dev_filepaths(data_dir, 1, shuffled=False)
        generator_utils.generate_dataset_and_shuffle(
            self.generator(True), train_paths,
            self.generator(False), dev_paths)

    def hparams(self, defaults, unused_model_hparams):
        p = defaults
        p.input_modality = {
            "inputs": (registry.Modalities.SYMBOL, 258),
            "inputs_second": (registry.Modalities.SYMBOL, 258),
        }
        p.target_modality = ("class_label:binary", 1)
        p.input_space_id = problem.SpaceID.EN_TOK
        p.target_space_id = problem.SpaceID.GENERIC

    def feature_encoders(self, data_dir):
        return {
            "inputs": text_encoder.ByteTextEncoder(),
            "inputs_second": text_encoder.ByteTextEncoder(),
            "targets": IdentityEncoder(),
        }

    def example_reading_spec(self):
        data_fields = {
            "inputs": tf.VarLenFeature(tf.int64),
            "inputs_second": tf.VarLenFeature(tf.int64),
            "targets": tf.FixedLenFeature([1], tf.float32),
        }
        data_items_to_decoders = None
        return data_fields, data_items_to_decoders


# Hack to use multiple inputs
def my_standardize_shapes(features, batch_size=None):
    for fname in {"inputs", "targets", "inputs_second"}.intersection(features):
        f = features[fname]
        while len(f.get_shape()) < 4:
            f = tf.expand_dims(f, axis=-1)
        features[fname] = f
    assert not batch_size
    return features


problem.standardize_shapes = my_standardize_shapes


# /Hack

@registry.register_hparams
def my_transformer_custom():
    hparams = transformer_base()
    hparams.num_hidden_layers = 1
    hparams.hidden_size = 32
    hparams.filter_size = 32
    hparams.max_length = 8
    hparams.num_heads = 4
    hparams.learning_rate_warmup_steps = 4000
    hparams.batch_size = 2048
    hparams.learning_rate = 0.025
    return hparams


class IdentityEncoder(object):
    def encode(self, x):
        print('encode', x)
        return x

    def decode(self, x):
        print('decode', x)
        return x


@registry.register_class_label_modality("binary")
class ClassLabelModality(modality.Modality):
    @property
    def name(self):
        return "class_label_modality_%d_%d" % (self._vocab_size, self._body_input_depth)

    def bottom(self, x):
        assert False

    def targets_bottom(self, x):
        with tf.variable_scope(self.name):
            return tf.zeros([common_layers.shape_list(x)[0], 1, 1, self._body_input_depth])

    def top(self, body_output, _):
        """Transform inputs from model space to target space.

        Average over inner dims and a linear layer to logits.

        Args:
          body_output: A Tensor with shape [batch, ?, ?, body_output_size].

        Returns:
          a Tensors, each with shape [batch_size, ?, ?, vocab_size]
        """
        with tf.variable_scope(self.name):
            x = body_output
            print('before x', x)
            x = tf.reduce_mean(x, axis=[1, 2], keep_dims=True)
            print('after x', x)
            res = tf.layers.dense(x, 1)
            return tf.expand_dims(res, 3)

    def loss(self, logits, targets):
        print('targets', targets)
        print('logits', tf.squeeze(logits, axis=3))
        return tf.losses.log_loss(
            tf.cast(targets, tf.float32),
            tf.sigmoid(tf.squeeze(logits, axis=3))
        ), tf.constant(0., tf.float32)
