import tensorflow as tf
from tensor2tensor.utils import registry
from tensor2tensor.utils import t2t_model
from tensor2tensor.layers import common_layers
from tensor2tensor.layers import common_attention
from tensor2tensor.models.transformer import transformer_encoder


@registry.register_model
class MyTransformerEncoder(t2t_model.T2TModel):
    def body(self, features):
        hparams = self._hparams
        target_space = features["target_space_id"]
        inputs0 = common_layers.flatten4d3d(features["inputs"])
        inputs1 = common_layers.flatten4d3d(features["inputs_second"])
        encoder_input0, encoder_self_attention_bias0 = my_transformer_prepare_encoder(inputs0, target_space, hparams)
        encoder_input1, encoder_self_attention_bias1 = my_transformer_prepare_encoder(inputs1, target_space, hparams)
        encoder_input0 += tf.constant([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                                       0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., ])
        encoder_input1 += tf.constant([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                                       0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., ])
        print('encoder_self_attention_bias0', encoder_self_attention_bias0)
        print('encoder_input0', encoder_input0)
        encoder_input = tf.concat([encoder_input0, encoder_input1], axis=1)
        encoder_self_attention_bias = tf.concat([encoder_self_attention_bias0, encoder_self_attention_bias1], axis=3)
        print('encoder_self_attention_bias', encoder_self_attention_bias)
        print('encoder_input', encoder_input)

        encoder_input = tf.nn.dropout(encoder_input, 1.0 - hparams.layer_prepostprocess_dropout)
        encoder_output = transformer_encoder(encoder_input, encoder_self_attention_bias, hparams)
        encoder_output = tf.expand_dims(encoder_output, 2)
        return encoder_output

    def infer(self,
              features=None,
              decode_length=50,
              beam_size=1,
              top_beams=1,
              alpha=0.0):
        """A inference method.

        Quadratic time in decode_length.

        Args:
          features: an map of string to `Tensor`
          decode_length: an integer.  How many additional timesteps to decode.
          beam_size: number of beams.
          top_beams: an integer. How many of the beams to return.
          alpha: Float that controls the length penalty. larger the alpha, stronger
            the preference for slonger translations.

        Returns:
           samples: an integer `Tensor`.
        """
        with self._var_store.as_default():
            # TODO(rsepassi): Make decoding work with real-valued model outputs
            # (i.e. if the target modality is RealModality).
            self.prepare_features_for_infer(features)
            if not self.has_input and beam_size > 1:
                tf.logging.warn("Beam searching for a model with no inputs.")
            if not self.has_input and self.hparams.sampling_method != "random":
                tf.logging.warn("Non-random sampling for a model with no inputs.")
            self._fill_problem_hparams_features(features)

            target_modality = self.hparams.problems[self._problem_idx].target_modality
            if target_modality.is_class_modality:
                beam_size = 1  # No use to run beam-search for a single class.
            assert beam_size == 1
            if beam_size == 1:
                tf.logging.info("Greedy Decoding")
                samples, logits, _ = self._greedy_infer(features, decode_length)
            else:
                tf.logging.info("Beam Decoding with beam size %d" % beam_size)
                samples = self._beam_decode(
                    features, decode_length, beam_size, top_beams, alpha)
            return tf.sigmoid(logits)  # tf.sigmoid(logits[:, :, :, :, 1])

def my_transformer_prepare_encoder(inputs, target_space, hparams, features=None):
    encoder_input = common_attention.add_timing_signal_1d(inputs)
    return encoder_input, common_attention.attention_bias_ignore_padding(common_attention.embedding_to_padding(inputs))
