import pandas as pd

df = pd.read_csv('base/data/test.csv')
df = df.applymap(lambda x: x.replace("\n", " ") if isinstance(x, str) else x)
df[['question1']].to_csv('base/data/q1.txt', index=False, header=False)
df[['question2']].to_csv('base/data/q2.txt', index=False, header=False)
#df[['is_duplicate']].to_csv('base/data/label.txt', index=False, header=False)
